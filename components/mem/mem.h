#include "types.h"

u32 memcpy(u8* src, u8* dest, const u32 size);

u32 memset(u8* buf, const u8 val, const u32 size);
