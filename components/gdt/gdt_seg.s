global load_gdt
extern gdt_ptr

load_gdt:
	mov ax, 0x10 	; offset in the gdt to our data segment
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	jmp 0x08:flush_cs ; far jump to the code segment

flush_cs:
	lgdt [gdt_ptr]
	ret 


