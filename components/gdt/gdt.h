#ifndef GDT_H
#define GDT_H
#include "types.h"

#define NO_GDT_ENTRIES 5

struct GDT_Entry {
	u16 low_limit;
	u16 low_base;
	u8 middle_base;
	u8 access;
	u8 granular;
	u8 high_base;
}__attribute__((packed));

// Contains the pointer to the first gdt entry as well as the size(of the whole table(?))
struct GDT_PTR {
	u16 size;
	u32 address;
}__attribute__((packed));

struct GDT_Entry gdt_entries[NO_GDT_ENTRIES];
struct GDT_PTR gdt_ptr;

// this func is actually taken care of in gdt_seg.s
extern void load_gdt();

void gdt_configure_entry(u32 entry, u32 base, u32 limit, u8 access, u8 gran);

void gdt_configure();

#endif
