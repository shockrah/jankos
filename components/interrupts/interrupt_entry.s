; Dispatching to our interrupt handler code from assembly because some of this is 
; enourmous pain in C

; Save the register state
; call the C handler
; executes iret(expects the stack to look like struct stack_state)


; If the interrupt we need to handle doesn't produce an error code then must provide a 0
; for consistency

extern interrupt_handler
extern cpu_reg_state
extern stack_state
extern idt_ptr
extern irq_handler

global load_idt
load_idt:
	lidt [idt_ptr]
	ret

%macro irq 2
global irq_handler_%1
irq_handler_%1:
	push dword 0
	push dword %2
	jmp common_irq_handler
%endmacro

%macro no_err_handler 1
global no_err_handler_%1 	; defined in interrupts.h
no_err_handler_%1:
	push dword 0
	push dword %1
	jmp common_int_handler
%endmacro

; deals with the intterrupts that do give us an error code
%macro err_code_handler 1
global err_code_handler_%1
err_code_handler_%1:
	push dword %1
	jmp common_int_handler
%endmacro

extern interrupt_handler ; external handler (interrupts.c)
common_int_handler:
	; save thigns before do our C-call
	pushad
	;push eax ecx edx ebx
	;push esp ebp esi edi

	push ds
	push es 
	push fs 
	push gs

	; load the kernel segment descriptor
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov eax, esp	; pushing the stack into the next call
	push eax
	mov eax, interrupt_handler
	call eax 		; preserve the eip register past this call

	; segments
	pop eax
	pop gs 
	pop fs
	pop es
	pop ds

	popad

	add esp, 8
	; special instruction which lets us jump back to code
	; which was previously interrupted
	iret 	


no_err_handler 0
no_err_handler 1
no_err_handler 2
no_err_handler 3
no_err_handler 4
no_err_handler 5
no_err_handler 6
no_err_handler 7

err_code_handler 8

no_err_handler 9

err_code_handler 10
err_code_handler 11
err_code_handler 12
err_code_handler 13
err_code_handler 14

no_err_handler 15
no_err_handler 16
no_err_handler 17
no_err_handler 18
no_err_handler 19
no_err_handler 20
no_err_handler 21
no_err_handler 22
no_err_handler 23
no_err_handler 24
no_err_handler 25
no_err_handler 26
no_err_handler 27
no_err_handler 28
no_err_handler 29
no_err_handler 30
no_err_handler 31


common_irq_handler:
    pusha
    push ds
    push es
    push fs
    push gs

    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    mov eax, esp
    push eax
    mov eax, irq_handler
    call eax
    pop eax

    pop gs
    pop fs
    pop es
    pop ds

    popa
    add esp, 8
    iret


; Remapped IRQ's for the APIC

; starting from irq 0 -> 15 but remapped to 32 -> 47
irq 0, 32
irq 1, 33
irq 2, 34
irq 3, 35
irq 4, 36
irq 5, 37
irq 6, 38
irq 7, 39
irq 8, 40
irq 9, 41
irq 10, 42
irq 11, 43
irq 12, 44
irq 13, 45
irq 14, 46
irq 15, 47