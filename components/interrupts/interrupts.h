#ifndef INTERRUPTS_H
#define INTERRUPTS_H
#include "types.h"

// There are more interrupts but tbh i doubt we're going to need them
#define i_Timer 0
#define i_Keyboard 1
#define i_PIC2 2
#define i_COM2 3
#define i_COM1 4
#define i_LPT2 5


struct cpu_reg_state {
	u32 gs, fs, es, ds; // segment registers
	u32 edi, esi, ebp, esp;	 // generaal purupose via pushad
	u32 ebx, edx, ecx, eax;
	u32 int_no, err_code; 	// pushed by each interrupt dispatcher
	u32 eip, cs, eflags, useresp, ss; 	// processor pushes this automatically
}__attribute__((packed)); // we shouldn't need packing since everythign is dwords here


#define IDT_SIZE 256

struct IDT_Entry {
	u16 offset_low; // offset bits 0..15
	u16 selector; 	// code segment in either gdt or ldt
	u8 zero; 		// not used always 0
	/* 7                           0
	 * +---+---+---+---+---+---+---+---+
	 * | P |  DPL  | S |    GateType   |
	 * +---+---+---+---+---+---+---+---+
	 */
	u8 type_attrs; 	// format specified above
	u16 offset_high;// offset bits 16..31
}__attribute__((packed));

struct IDT_PTR {
	u16 limit;
	u32 address;
}__attribute__((packed));

struct IDT_Entry IDT[IDT_SIZE];
struct IDT_PTR idt_ptr;

void init_idt();
void setup_idt_entry(u32 t_idx, u32 base, u16 sel, u8 type_attrs);
void int_keyboard(struct cpu_reg_state*);
void interrupt_handler(struct cpu_reg_state*);


// IRQ specifi functions
void init_irq_handler(u32 irq, void (*handler)(struct cpu_reg_state* cpu));
void irq_handler(struct cpu_reg_state* cpu);
#endif
