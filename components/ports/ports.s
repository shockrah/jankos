; Helpers for serial ports which pretty much have to to be 
; written in asm

global serialport_write_byte
global serial_read_byte

; write a byte  
;serialport_write_byte
serialport_write_byte:
	mov al, [esp+8]
	mov dx, [esp+4]
	out dx, al
	ret

; Read byte from serial port
;serialport_read_byte
serial_read_byte:
	mov dx, [esp + 4] 	; grab the address of the targeted port
	in al, dx 			; read byte from the port 
	ret

