// US Keyboard layout because 'Murrica
#include "kbd.h"
#include "ports.h"
#include "pit.h"
#include "serial.h"
#include "types.h"
#include "stlio.h"


// Courtesy of http://www.osdever.net/bkerndev/Docs/keyboard.htm
unsigned char keymap[128] = {
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '=', '\b',	/* Backspace */
  '\t',			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   0,		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/',   0,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};


// Reads one key from the keyboard
void kbd_read_key(__attribute__((unused)) struct cpu_reg_state* cpu) {
	u8 scancode = serial_read_byte(KBD_PORT);
	if(scancode & 0x80) {
		// Key released
		kbd_key = keymap[scancode & 0x7f]; // clamp to 127 as the max val(gcc is anoying)
		kbd_state = KBD_RELEASE;
		kbd_time = pit_timer_ticks;
	}
	else {
		// Key pressed
		kbd_key = keymap[scancode & 0x7f]; // clamp to 127 as the max val(gcc is anoying)
		kbd_state = KBD_PRESS;
		kbd_time = pit_timer_ticks;
	}
}

void kbd_install_keyboard(void) {
	init_irq_handler(1, kbd_read_key);
	kbd_key = '\0';
	kbd_state = 0;
	kbd_time = 0;
}
