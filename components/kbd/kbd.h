#ifndef KBD_H
#define KBD_H
#include "types.h"
#include "interrupts.h"
#include "framebuffer.h"

#define KBD_PORT 0x60

#define KBD_PRESS 1
#define KBD_WAITING 2
#define KBD_RELEASE 0x80

char kbd_key;
u8 kbd_state;
u32 kbd_time;

void kbd_install_keyboard(void);

void kbd_read_key(struct cpu_reg_state* cpu);
#endif
