#include "types.h"
#include "serial.h"
#include "ports.h"
#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#define COLUMNS 80
#define ROWS 25
#define AREA ( COLUMNS * ROWS )

// frame buffer port commansd
#define FB_CMD 0x3d4
#define FB_DATA 0x3d5

#define FB_HIGH_CMD 14
#define FB_LOW_CMD 15

// address of our frame buffer
#define FRAME_BUFFER_ADDR 0x000B8000

// Logical index of a cell in the frame buffer
#define FRAME_CELL(cell_) (cell_ * 2)

// Colors available in text framebuffer
#define Black 0x01
#define Blue 0x02
#define Green 0x03
#define Cyan 0x04
#define Red 0x05
#define Magenta 0x05
#define Brown 0x06
#define LightGrey 0x07
#define DarkGrey 0x08
#define LightBlue 0x09
#define LightGreen 0x0a
#define LightCyan 0x0b
#define LightRed 0x0c
#define LightMagenta 0x0d
#define LightBrown 0x0e
#define White 0x0f

extern s32 Frame_Buffer_Cursor;

void frame_buffer_newline();

void write_cell_fb(u8, u8, u8);

void clear_fb(void);

void fb_move_cursor(u16);

#endif
