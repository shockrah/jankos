// Driver implementation for Jank's text frame buffer

// Buffer can display 80 columns by 25 Rows
// Start of buffer is at (real)address 0x000B8000
// Memory at this section is divided into 16-bit cells 
    // | 15-8 | 7-4 | 0-3
    // | Asci | FG  | BG
#include "types.h"
#include "framebuffer.h"

// State trackers from header
static u8* Frame_Buffer = (u8*)FRAME_BUFFER_ADDR;
s32 Frame_Buffer_Cursor = 0x0000;

void frame_buffer_newline() {
	while(Frame_Buffer_Cursor != (AREA*8)) {
		if((Frame_Buffer_Cursor % 160) == 0) {
			break;
			Frame_Buffer_Cursor+=160;
		}
		else {
			Frame_Buffer_Cursor+=2;
		}
	}
}
// Writes character to a given cell in the framebuffer
    // @cell parameter is the logical (linear)index into the buffer
void write_cell_fb(u8 c, u8 fg, u8 bg) {
    Frame_Buffer[Frame_Buffer_Cursor] = c;
    Frame_Buffer[Frame_Buffer_Cursor+1] = (fg & 0x0f << 4) | (bg & 0x0f);
	Frame_Buffer_Cursor += 2;
}

void clear_fb(void) {
	for(unsigned cell=0;cell<AREA; cell+=2) {
		write_cell_fb(' ', 0x00, 0x00);
	}
	Frame_Buffer_Cursor = 0x0000;
}

// Takes the linear indexed position to move the cursos, if there is nothing at that position we should still have something there
void fb_move_cursor(u16 position) {
	serialport_write_byte(FB_CMD, FB_HIGH_CMD);
	serialport_write_byte(FB_DATA, ((position >> 8) & 0x00ff) );
	serialport_write_byte(FB_CMD, FB_LOW_CMD);
	serialport_write_byte(FB_DATA, position & 0x00ff);
}

