# JankOS

**[Download a bootable usb here](https://gitlab.com/shockrah/jankos/-/jobs/artifacts/release/download?job=all)**

The best operating system you've ever laid eyes on.
It's so good that barely does anything at all.

JankOS serves the purpose of an experiment in learning.
Personally I'm unsatisfied with accepting an idea; I want to garner an intuition for the inner workings of an idea.

## Done & Dusted 

* GDT Initialization - now ready to take steps to move to user mode

* IDT Initialization

## Todo/Roadmap

* JShell - a _sorta_ shell implementation

* Fixing Interrupt handlers - IRQ's now need to be implemented

* Memory allocator

* Paging will likely come before the allocator: If segmentation isn't being done then _paging_ will be part of the roadmap(most likely)

## Bugs/TODOs

* Oranizing this codebase: because its a giant mess flat directory type thing.Although this project is more for me than anyone else so I suppose it doesn't matter too much.

* `serial::serial_write(const char*, const u64)` doesn't always write all the required data nor does it properly deal with the FIFO queue associated with the COM1 port.

* `stlio::printf` still isn't feature complete

* `tests::*` could always use more tests to cover as much of the code base as possible.

* Interrupts will often refire over and over again for no discernable reason(tested with Divide by Zero so far)

